const IfOuter = {
  bind (el, binding) {
    if (binding.value) return

    while (el.childNodes.length) {
      el.parentNode.insertBefore(el.childNodes[0], el.nextSibling)
    }

    el.parentNode.removeChild(el)
  }
}

export default {
  IfOuter
}
